<?php
/*
Write simple .camelCase method (camel_case function in PHP or CamelCase in C#) for strings. All words must have their first letter capitalized without spaces.

For instance:

camel_case("hello case"); // => "HelloCase"
camel_case("camel case word"); // => "CamelCaseWord"
Don't forget to rate this kata! Thanks :)
*/

function camel_case(string $s): string {
  $strings = explode(" ", trim($s));
  foreach($strings as $k=>$r){
    $strings[$k] = ucfirst($r);
  }
  return implode("", $strings);
}

/*
BEST

function camel_case(string $s): string {
  return str_replace(' ', '', ucwords(trim($s)));
}
*/

/*
ucwords??
ucwords — Uppercase the first character of each word in a string

<?php
$foo = 'hello world!';
$foo = ucwords($foo);             // Hello World!

$bar = 'HELLO WORLD!';
$bar = ucwords($bar);             // HELLO WORLD!
$bar = ucwords(strtolower($bar)); // Hello World!
?>

*/
?>
