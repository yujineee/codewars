<?php
// Compare two strings by comparing the sum of their values (ASCII character code).
// For comparing treat all letters as UpperCase.
//
// Null-Strings should be treated as if they are empty strings.
// If the string contains other characters than letters, treat the whole string as it would be empty.
//
// Examples:
//
// "AD","BC" -> equal
//
// "AD","DD" -> not equal
//
// "gf","FG" -> equal
//
// "zz1","" -> equal
//
// "ZzZz", "ffPFF" -> equal
//
// "kl", "lz" -> not equal
//
// null, "" -> equal
//
// Your method should return true, if the strings are equal and false if they are not equal.

function compare($s1, $s2) {
    $sum1 = 0;
    $sum2 = 0;

    $upperS1 = strtoupper($s1);
    $upperS2 = strtoupper($s2);

    preg_match('/[A-Z]+/', $upperS1, $matchS1);
    if(!empty($matchS1)){
        if(strlen($matchS1[0]) != strlen($upperS1)){
          $upperS1 = "";
        }else{
            for($i = 0 ; $i < strlen($matchS1[0]); $i++){
                $sum1 += ord($matchS1[0]{$i});
            }
        }

    }


    preg_match('/[A-Z]+/', $upperS2, $matchS2);
    if(!empty($matchS2)){
        if(strlen($matchS2[0]) != strlen($upperS2)){
          $upperS2 = "";
        }else{
           for($i = 0 ; $i < strlen($matchS2[0]); $i++){
              $sum2 += ord($matchS2[0]{$i});
           }
        }

    }
    //var_dump($matchS1);
    //정규식으로 구분


    echo $sum1." ".$sum2."<br>";
    if($sum1 == $sum2){
        return true;
    }else{
        return false;
    }

}

/*
function getCharValues($string)
{
  if ( !$string || preg_match('/[^A-Z]{1,}/', $string) ) return 0;

  return array_sum(
    array_map(
      function($item){
          return ord($item);
      },
      str_split($string)
    )
  );
}

function compare($s1, $s2)
{
  return getCharValues(strtoupper($s1)) == getCharValues(strtoupper($s2));
}
*/
?>
