<?php
function reverse(array $a): array {

  $length = sizeof($a)-1;
  $reverseArray = array();
  foreach ($a as $k=>$r) {
    $reverseArray[$length-$k] = $r;
  }
  ksort($reverseArray);
  return $reverseArray;
}

?>
function reverse(array $a): array {
  $b = [];
  foreach ($a as $val){
  $b[] = array_pop($a);
  }
  return $b;
}
