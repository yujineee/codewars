<?php
/*
Description:

We want to generate a function that computes the series starting from 0 and ending until the given number following the sequence:

0 1 3 6 10 15 21 28 36 45 55 ....

which is created by

0, 0+1, 0+1+2, 0+1+2+3, 0+1+2+3+4, 0+1+2+3+4+5, 0+1+2+3+4+5+6, 0+1+2+3+4+5+6+7 etc..

Input:

LastNumber

Output:

series and result

Example:

Input:

> 6
Output:

0+1+2+3+4+5+6 = 21
Input:

> -15
Output:

-15<0
Input:

> 0
Output:

0=0
*/

/*
source_code
*/
class SequenceSum {
  public function showSequence($count) {
     $query = '';
     if($count < 0){
       $query = $count."<0";
     }else if($count == 0){
       $query = "0=0";
     }else{
         $string_array = array();
         $sum = 0;
         for($i = 0; $i <= $count; $i++){
             array_push($string_array,$i);
             $sum += $i;
         }
         $query = implode("+", $string_array);
         $query .=" = ".$sum;
     }


     return $query;
  }
}

/*Time: 297ms Passed: 1 Failed: 0*/

/*
BEST SOLUTION

class SequenceSum {
  public function showSequence($count) {
    if ($count < 0) return $count . '<0';
    if ($count == 0) return '0=0';
    return implode("+", range(0, $count)) . ' = ' . ($count * ($count + 1) / 2);
  }
}

//range를 통해서 0123456을 array로 받아서 implode 시켰따.
sum을 n*n+1 /2 공식으로 구했따.
멋지넹...

*/
?>
