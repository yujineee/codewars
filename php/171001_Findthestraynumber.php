<?php
/*
You are given an odd-length array of integers, in which all of them are the same, except for one single number.

Implement the method stray which accepts such array, and returns that single different number.

The input array will always be valid! (odd-length >= 3)

Examples:

[1, 1, 2] => 2

[17, 17, 3, 17, 17, 17, 17] => 3

**test codes **

$this->assertEquals(stray([1, 1, 2]), 2);
$this->assertEquals(stray([4, 2, 2, 2, 2]), 4);
$this->assertEquals(stray([4, 4, 4, 5, 4, 4, 4]), 5);

*/

function stray($arr)
{
    $count = array_count_values($arr);
    return array_search(1, $count);
}


/*
Clever Sample Code

function stray(array $arr) {
  return array_search(1, array_count_values($arr));
}

it is same of mine. But looks better than mine. because it is more simiple.

*/

/*Results*/

// Time: 261ms Passed: 2 Failed: 0


?>
