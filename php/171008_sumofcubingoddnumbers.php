function cube_odd($a) {
  // Your code here
  $result = 0;
  print_r($a);
  foreach($a as $k=>$v){
    //echo ($v%2)."<br>";
    if(!is_int($v)){
      $result = null;
      break;
    }
    if($v%2 == 1 || $v%2 == -1){
      $cubing = $v*$v*$v;
      $result += $cubing;
    }else{
      continue;
    }

  }
  return $result;
}

/*
best SOLUTION
function is_odd($n) {
  return $n % 2 != 0;
}

function cube($n) {
  return pow($n, 3);
}

function cube_odd($a) {
  return sizeof(array_filter($a, "is_numeric")) == sizeof($a)
    ? array_sum(array_map("cube", array_filter($a, "is_odd")))
    : null;
}
*/
