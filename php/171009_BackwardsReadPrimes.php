<?php
/*
Backwards Read Primes are primes that when read backwards in base 10 (from right to left) are a different prime. (This rules out primes which are palindromes.)

Examples:
13 17 31 37 71 73 are Backwards Read Primes
13 is such because it's prime and read from right to left writes 31 which is prime too. Same for the others.

Task

Find all Backwards Read Primes between two positive given numbers (both inclusive), the second one being greater than the first one. The resulting array or the resulting string will be ordered following the natural order of the prime numbers.

Example

backwardsPrime(2, 100) => [13, 17, 31, 37, 71, 73, 79, 97] backwardsPrime(9900, 10000) => [9923, 9931, 9941, 9967]

backwardsPrime(2, 100) => [13, 17, 31, 37, 71, 73, 79, 97]
backwardsPrime(9900, 10000) => [9923, 9931, 9941, 9967]
**/

function isPrime($n){
    $prime = true;
    //수행시간이 너무 길어서 timeout현상 발생
    //소수를 찾는법의 문제 -> trial division 선택하여 변경

    //http://soyoja.com/160
    for($j = 2; $j*$j<=$n;$j++){
        if($n%$j == 0){
            $prime = false;
            break;
        }
    }
    return $prime;
}

function backwardsPrime($start, $stop){
  $backwardsPrime = array();
  for($i = $start; $i<= $stop; $i++){
    $reverse = strrev($i);
    if($reverse!=$i && isPrime($i) && isPrime($reverse)){
        array_push($backwardsPrime, $i);
    }
  }

  return $backwardsPrime;
}
?>
