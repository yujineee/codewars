<?php
//
// Description:
// Given an array (arr) as an argument complete the function countSmileys that should return the total number of smiling faces.
//
// Rules for a smiling face:
// -Each smiley face must contain a valid pair of eyes. Eyes can be marked as : or ;
// -A smiley face can have a nose but it does not have to. Valid characters for a nose are - or ~
// -Every smiling face must have a smiling mouth that should be marked with either ) or D.
// No additional characters are allowed except for those mentioned.
// Valid smiley face examples:
// :) :D ;-D :~)
// Invalid smiley faces:
// ;( :> :} :]
//
// Example cases:
//
// countSmileys([':)', ';(', ';}', ':-D']);       // should return 2;
// countSmileys([';D', ':-(', ':-)', ';~)']);     // should return 3;
// countSmileys([';]', ':[', ';*', ':$', ';-D']); // should return 1;
//
// Note: In case of an empty array return 0. You will not be tested with invalid input (input will always be an array)
//
// Happy coding!

function count_smileys($arr): int {
  $validSmile = array_filter($arr, "isValidSmile");
  return count($validSmile);
}

function isValidSmile($face){
  $eye = substr($face, 0, 1);
  $mouth = substr($face, -1);

  if(strlen($face)==3){
    $nose = substr($face, 1, 1);
    if(isEye($eye)&&isMouth($mouth)&&isNose($nose)) return true;
    else return false;
  }else if(strlen($face)==2){
    if(isEye($eye)&&isMouth($mouth)) return true;
    else return false;
  }else{
    return false;
  }
}


function isEye($eye){
  if($eye == ':' || $eye == ';') return true;
  else return false;
}

function isNose($nose){
  if($nose == '-' || $nose == '~') return true;
  else return false;
}


function isMouth($mouth){
  if($mouth == ')' || $mouth == 'D') return true;
  else return false;
}
?>
