<?php
// Given a number, return a string with dash'-'marks before and after each odd integer, but do not begin or end the string with a dash mark.
//
// Ex:
//
// dashatize(274) -> '2-7-4'
// dashatize(6815) -> '68-1-5'

//맘에 안든다...

function dashatize(int $num): string {
    if($num<0){
        $num = $num * -1;
    }

    $num = strval($num);
    $tempString = '';
    $dashArr = array();
    for($i = 0; $i<strlen($num); $i++){
        if($num{$i}%2 == 1){
            if($tempString!= ''){
                array_push($dashArr, $tempString);
                $tempString = '';
            }
            array_push($dashArr, $num{$i});
        }else{
            $tempString .= $num{$i};
            if($i == strlen($num)-1){
                if($tempString!= ''){
                    array_push($dashArr, $tempString);
                    $tempString = '';
                }
            }
        }
    }

    return implode('-',$dashArr);
}

//BEST1
function dashatize(int $num): string {

    return trim(str_replace('--', '-', preg_replace('/([13579])/','-$1-', $num)), '-');
}

//BEST2
function dashatize(int $num): string {
  $res  = str_split($num);
  $result = '';
  foreach ($res as $d){
    $result .= ($d % 2) ? '-' . $d . '-' : $d;
  }

  return str_replace('--', '-', rtrim(ltrim($result, '-'), '-'));
}
?>
