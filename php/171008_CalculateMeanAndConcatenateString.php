<?php
/*
You will be given an array which will include both integers and characters.

Return an array of length 2 with a[0] representing the mean of the ten integers as a floating point number. There will always be 10 integers and 10 characters. Create a single string with the characters and return it as a[1] while maintaining the original order.

lst = ['u', '6', 'd', '1', 'i', 'w', '6', 's', 't', '4', 'a', '6', 'g', '1', '2', 'w', '8', 'o', '2', '0']
Here is an example of your return

[3.6, "udiwstagwo"]
In C# and Java the mean return is a double.
*/
function is_str($n){
    if(!is_numeric($n)&&is_string($n)){
        return true;
    }else{
        return false;
    }
}

function mean(array $a): array {
    $num = array_filter($a, "is_numeric");
    $mean = (array_sum($num))/10;

    $string = array_filter($a, "is_str");

    return [$mean , implode("",$string)];
}

/*
BEST

function mean(array $a): array
{
    $sum = 0;
    $s = '';
    foreach ($a as $item) {
        if (is_numeric($item)) {
            $sum += $item;
        } else {
            $s .= $item;
        }
    }
    return [$sum / 10, $s];
}

*/
?>
